<?php
/**
 * Queremos crear un hash de nuestra contraseña uando el algoritmo DEFAULT actual.
 * Actualmente es BCRYPT, y producirá un resultado de 60 caracteres.
 *
 * Hay que tener en cuenta que DEFAULT puede cambiar con el tiempo, por lo que debería prepararse
 * para permitir que el almacenamento se amplíe a más de 60 caracteres (255 estaría bien)
 */
 
$hash1 = password_hash("123456", PASSWORD_BCRYPT);

$hash2 = password_hash("123456", PASSWORD_DEFAULT);

echo ("Esto es el hash1: " .$hash1 ."\n");

echo ("Esto es el hash2: " .$hash2 ."\n");



if (password_verify("MiPassword", $hash1)) {
    echo '¡La contraseña hash1 es válida!' ."\n";
} else {
    echo 'La contraseña hash1 no es válida.' ."\n";
}


if (password_verify("Mipasword", $hash2)) {
    echo '¡La contraseña hash2 es válida!' ."\n";
} else {
    echo 'La contraseña hash2 no es válida.' ."\n";
}

?>