-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 23-03-2021 a las 20:09:30
-- Versión del servidor: 10.4.14-MariaDB-cll-lve
-- Versión de PHP: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


CREATE TABLE `departamento` (
  `departamentoid` int(5) NOT NULL,
  `nombre` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Estructura de tabla para la tabla `localidad`
--

CREATE TABLE `localidad` (
  `localidadid` int(5) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `departamentoid` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE `categoria` (
  `categoriaid` int(5) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `descripcion` text DEFAULT NULL,
  `urlfoto` varchar(255) DEFAULT NULL,
  `categoriapadre` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
--
-- Estructura de tabla para la tabla `newsletter`
--

CREATE TABLE `newsletter` (
  `newsletterid` int(11) NOT NULL,
  `correo` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Estructura de tabla para la tabla `tienda`
--

CREATE TABLE `tienda` (
  `tiendaid` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `descripcion` text NOT NULL,
  `urlfoto` varchar(255) DEFAULT NULL,
  `slug` varchar(150) NOT NULL,
  `fechacreacion` date DEFAULT NULL,
  `fechaactualizado` date DEFAULT NULL,
  `categoriaid` int(5) DEFAULT NULL,
  `localidadid` int(5) DEFAULT NULL,
  `estado` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE `tiendafotos` (
  `tiendafotoid` int(11) NOT NULL,
  `tiendaid` int(11) NOT NULL,
  `urlfoto` varchar(255) DEFAULT NULL,
  `descripcion` varchar(150) NOT NULL,
  `pos` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `usuarioid` int(11) NOT NULL,
  `correo` varchar(60) DEFAULT NULL,
  `nombre` varchar(60) DEFAULT NULL,
  `direccion` varchar(60) DEFAULT NULL,
  `telefono` varchar(20) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `fechacreacion` date DEFAULT NULL,
  `fechaactualizado` date DEFAULT NULL,
  `estado` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `usuarios` (`usuarioid`, `correo`, `password`, `estado`) VALUES
(1, 'usuario1@gmail.com', '$2y$10$c1u5UZY7Wy5mkIow0sc.BuHVn4E7ZkPp355eQ69u3IND.c3I/kOKu', 'Activo'),
(2, 'usuario2@gmail.com', '$2y$10$c1u5UZY7Wy5mkIow0sc.BuHVn4E7ZkPp355eQ69u3IND.c3I/kOKu', 'Activo'),
(3, 'usuario3@gmail.com', '$2y$10$c1u5UZY7Wy5mkIow0sc.BuHVn4E7ZkPp355eQ69u3IND.c3I/kOKu', 'Activo'),
(4, 'usuario4@gmail.com', '$2y$10$c1u5UZY7Wy5mkIow0sc.BuHVn4E7ZkPp355eQ69u3IND.c3I/kOKu', 'Activo');

--
-- Estructura de tabla para la tabla `usuarios_token`
--

CREATE TABLE `usuarios_token` (
  `tokenid` int(11) NOT NULL,
  `usuarioid` int(11) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `estado` varchar(45) CHARACTER SET armscii8 DEFAULT NULL,
  `fecha` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Índices para tablas volcadas
--


ALTER TABLE `departamento`
  ADD PRIMARY KEY (`departamentoid`);

ALTER TABLE `localidad`
  ADD PRIMARY KEY (`localidadid`);

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`categoriaid`);

--
-- Indices de la tabla `newsletter`
--
ALTER TABLE `newsletter`
  ADD PRIMARY KEY (`newsletterid`);


--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`usuarioid`);
  
  
ALTER TABLE `tienda`
  ADD PRIMARY KEY (`tiendaid`);

ALTER TABLE `tiendafotos`
  ADD PRIMARY KEY (`tiendafotoid`);

--
-- Indices de la tabla `producto`
--

ALTER TABLE `usuarios_token`
  ADD PRIMARY KEY (`tokenid`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `categoriaid` int(5) NOT NULL AUTO_INCREMENT;
  
  
ALTER TABLE `departamento`
  MODIFY `departamentoid` int(5) NOT NULL AUTO_INCREMENT;

ALTER TABLE `localidad`
  MODIFY `localidadid` int(5) NOT NULL AUTO_INCREMENT;



ALTER TABLE `newsletter`
  MODIFY `newsletterid` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `tienda`
  MODIFY `tiendaid` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `tiendafotos`
  MODIFY `tiendafotoid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuarios_token`
--

ALTER TABLE `usuarios_token`
  MODIFY `tokenid` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `usuarios`
  MODIFY `usuarioid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;

INSERT INTO `departamento` (`nombre`) VALUES
('Montevideo'),
('Canelones'),
('Maldonado'),
('San Jose'),
('Flores'),
('Florida'),
('Colonia'),
('Durazno'),
('Trainta y Tres'),
('Rivera'),
('Artigas'),
('Salto'),
('Paysandu'),
('Cerro Largo'),
('Tacuarembo'),
('Soriano'),
('Lavalleja');

COMMIT;